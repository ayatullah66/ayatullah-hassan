#include "mathematics.hpp" // for mathematics::heron
#include <iostream> // for std::cout
#include <algorithm> // for std::atof

#include <string>
int main(int argc, char **argv)
{

    double a = std::atof(argv[1]); // a,b and c are the sides of the triangle
    double b = std::atof(argv[2]);
    double c = std::atof(argv[3]);
    double area = mathematics::heron(a, b, c);
    std::cout << "Area =  " << area << std::endl;
}

