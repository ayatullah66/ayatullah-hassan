//
// Created by asem on 01/04/18.
//

#include "set.hpp"
#include "helpers2.hpp"


int main( int argc, char **argv )
{
    if( argc == 2 )
    {
        std::vector< std::string > words = getFileWords( argv[1] );

        // COMPLETE HERE
set::WordSet wset = set::create();    
    int countwords=0;    
  for(int i=0; i<words.size();i++)
   {  
       if (words[i] != words[i+1]) //dont need it but just make sure of something during  running
       {
           countwords++;
       }
       set::insert(wset,words[i]);
   }
   std::cout<< "Number of unique words= "<<countwords <<std::endl;
   set::printAll( wset );

        // DONE HERE
    }

    return 0;
}
