//
// Created by asem on 01/04/18.
//
#include "map.hpp"
#include "helpers2.hpp"
#include <iostream>

int main(int argc, char **argv)
{
    if (argc == 2)
    {
        std::vector<std::string> words = getFileWords(argv[1]);
        int word_counter = 0;

        for (int i = 0; i < words.size(); ++i)
        {
            if (words[i] != " ")
            {
                word_counter++;
            }
        }

        std ::cout << "Total count of words = " << word_counter - 1 << std ::endl;
        //I have removed 1 from word_counter because the last element in string equal null and word_counter count it as a word
    }

    return 0;
}
