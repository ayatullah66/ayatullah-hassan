#ifndef DNA_HPP
#define DNA_HPP

#include "arrays.hpp"
namespace dna
{

char complementaryBase(char base)
{
  switch (base)
  {
  case 'A':
  {
    return 'T';
    break;
  }
  case 'T':
  {
    return 'A';
    break;
  }
  case 'C':
  {
    return 'G';
    break;
  }
  default:
  {
    return 'C';
  }
  }
}
char *complementarySequence(char *base, int size)
{

  char *complementary_Sequence = new char[size];
  int i, j;
  for (i = 0, j = size-1; (i < size && j > 0); i++, j--)
  {
    complementary_Sequence[i] = base[j];

    complementary_Sequence[i] = complementaryBase(complementary_Sequence[i]);
  }

  return complementary_Sequence;
}
char *analyzeDNA(char *base, int size, int &countA, int &countC, int &countG, int &countT)
{
  countA = arrays::countCharacter(&base[0], size, 'A');
  countC = arrays::countCharacter(&base[0], size, 'C');
  countG = arrays::countCharacter(&base[0], size, 'G');
  countT = arrays::countCharacter(&base[0], size, 'T');

  return complementarySequence(&base[0], size);
}
}



#endif // DNA_HPP
