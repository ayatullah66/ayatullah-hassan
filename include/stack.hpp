#ifndef STACK_HPP
#define STACK_HPP
#include <iostream>
//namespace for character stack using arrays.
namespace Cstack
{
struct CharStack1000
{
    char buffer[1000];
    int top = -1;
};

void push(CharStack1000 &stack, char newElement)
{
    ++stack.top;
    stack.buffer[stack.top] = newElement;
}

char pop(CharStack1000 &stack)
{
    char lifo = stack.buffer[stack.top];
    --stack.top;
    return lifo;
}

bool isEmptyStack(CharStack1000 &stack)
{
    return (stack.top == -1);
}
int size(CharStack1000 &stack)
{
    return (stack.top + 1);
}
void clear(CharStack1000 &stack)
{
    stack.top = -1;
}
}
//namespace for Integer stack using arrays.
//.........................................
//.........................................
namespace Istack
{
struct IStack1000
{
    int buffer[1000];
    int top = -1;
};

void push(IStack1000 &stack, int newElement)
{
    ++stack.top;
    stack.buffer[stack.top] = newElement;
}

int pop(IStack1000 &stack)
{
    int lifo = stack.buffer[stack.top];
    --stack.top;
    return lifo;
}

bool isEmptyStack(IStack1000 &stack)
{
    return (stack.top == -1);
}
int size(IStack1000 &stack)
{
    return (stack.top + 1);
}
void clear(IStack1000 &stack)
{
    stack.top = -1;
}
}
#endif // STACK_HPP
