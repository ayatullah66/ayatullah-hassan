#ifndef ARRAYS_HPP
#define ARRAYS_HPP

#include <iostream>
#include "mathematics.hpp"
namespace arrays
{

void printAll(double *base, int arraySize)
{

  for (int i = 0; i < arraySize; i++)
  {
    std::cout << base[i] << std::endl;
  }
}
double maxArray(double *base, int arraySize)
{
  double maxelement = base[0];
  for (int i = 1; i < arraySize; i++)
  {

    if (base[i] > maxelement)
    {
      maxelement = base[i];
    }
  }
  return maxelement;
}

double minArray(double *base, int arraySize)
{
  double minielement = base[0];
  for (int i = 1; i < arraySize; i++)
  {

    if (base[i] < minielement)
    {
      minielement = base[i];
    }
  }
  return minielement;
}

double meanArray(double *base, int arraySize)
{
  double sum = 0;
  for (int i = 0; i < arraySize; i++)
  {
    sum = sum + base[i];
  }
  return sum / arraySize;
}

double varianceArray(double *base, int arraySize)
{
  double sum = 0;
  double deviation = 0;
  double mean = arrays::meanArray(&base[0], arraySize);
  for (int i = 0; i < arraySize; i++)
  {
    deviation = mean - base[i];
    sum = sum + mathematics::square(deviation);
  }
  return sum / arraySize;
}
int countCharacter(char *basePointer, int size, char query)
{
  int counter = 0;
  for (int i = 0; i < size; i++)
  {
    if (basePointer[i] == query)
    {
      counter++;
    }
  }

  return counter;
}
}

#endif // ARRAYS_HPP

